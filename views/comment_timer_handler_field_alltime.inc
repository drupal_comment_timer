<?php
// vim: set ft=php syntax=php expandtab ts=2 sw=2 autoindent smartindent:

/**
 * @file @TODO
 */

/**
 * Views field handler for the Comment Timer's all time field.
 */
class comment_timer_handler_field_alltime extends views_handler_field_numeric {
  function query() {
    $this->add_additional_fields(array('comment_timer_seconds_alltime' => 'seconds', 'comment_timer_nodeseconds_alltime' => 'nodeseconds'));
  }

  function render($values) {
    $alltime = $values->seconds + $values->nodeseconds;
    if ($this->options['format']) {
      return $alltime;;
    }
    return _comment_timer_seconds_to_hms($alltime);
  }

  function access() {
    if (!user_access('access comment timer')) {
      // User is not permitted to access comment timer.
      return FALSE;
    }
    return parent::access();
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['format'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display as seconds'),
      '#description' => t('Time will be displayed as seconds when enabled (instead of HH:MM:SS).'),
      '#default_value' => $this->options['format'],
    );
  }
}
