<?php
// vim: set ft=php syntax=php expandtab ts=2 sw=2 autoindent smartindent:

/**
 * @file
 * Provide views data and views handlers for comment_timer.module.
 */

 /**
 * Implementation of hook_views_data().
 */
function comment_timer_views_data() {
  $data['comment_timer_comment']['table']['group'] = t('Comment');
  $data['comment_timer_comment']['table']['join'] = array(
    'comments' => array(
      'left_field' => 'cid',
      'field' => 'cid',
      'required' => TRUE,
    ),
  );
  $data['comment_timer_comment']['seconds'] = array(
    'title' => t('Time'),
    'help' => t('Time spent with the comment.'),
    'field' => array(
      'handler' => 'comment_timer_handler_field_seconds',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['comment_timer_node']['table']['group'] = t('Node');
  $data['comment_timer_node']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
      'required' => TRUE,
    ),
  );
  $data['comment_timer_node']['seconds'] = array(
    'title' => t('Comment Time'),
    'help' => t('Total time spent with the node\'s comments.'),
    'field' => array(
      'handler' => 'comment_timer_handler_field_seconds',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['comment_timer_node']['nodeseconds'] = array(
    'title' => t('Node time'),
    'help' => t('Total time spent with the node.'),
    'field' => array(
      'handler' => 'comment_timer_handler_field_seconds',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['comment_timer_node']['alltime'] = array(
    'title' => t('All time'),
    'help' => t('Total time spent with the node and it\'s comments.'),
    'field' => array(
      'handler' => 'comment_timer_handler_field_alltime',
      'notafield' => TRUE,
    ),
    // @TODO: Provide a solution to be able to sort on this "field".
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  return $data;
}

// @TODO: Use hook_views_data_alter() to add
// 'Whether the comment has timer information'.

/**
 * Implementation of hook_views_handlers().
 */
function comment_timer_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'comment_timer') .'/views',
    ),
    'handlers' => array(
      'comment_timer_handler_field_seconds' => array(
        'parent' => 'views_handler_field_numeric',
      ),
      'comment_timer_handler_field_alltime' => array(
        'parent' => 'views_handler_field_numeric',
      ),
    ),
  );
}
