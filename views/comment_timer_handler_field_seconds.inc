<?php
// vim: set ft=php syntax=php expandtab ts=2 sw=2 autoindent smartindent:

/**
 * @file @TODO
 */

/**
 * Views field handler for the Comment Timer's seconds field.
 */
class comment_timer_handler_field_seconds extends views_handler_field_numeric {
  function render($values) {
    if ($this->options['format']) {
      return $values->{$this->field_alias};
    }
    return _comment_timer_seconds_to_hms($values->{$this->field_alias});
  }

  function access() {
    if (!user_access('access comment timer')) {
      // User is not permitted to access comment timer.
      return FALSE;
    }
    return parent::access();
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['format'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display as seconds'),
      '#description' => t('Time will be displayed as seconds when enabled (instead of HH:MM:SS).'),
      '#default_value' => $this->options['format'],
    );
  }
}
