// vim: set ft=javascript syntax=javascript expandtab ts=2 sw=2 autoindent smartindent:

/**
 * Allow user to start or stop the timer.
 */
function comment_timer_pause() {
  if (Drupal.settings.comment_timer.running) {
    Drupal.settings.comment_timer.running = 0;
    window.setTimeout(function() {
      delay();
    }, 1000);
  }
  else {
    if (Drupal.settings.comment_timer.semaphore) {
      Drupal.settings.comment_timer.running = 1;
      Drupal.settings.comment_timer.semaphore = 0;
      comment_timer_timer();
    }
  }
}

/**
 * Prevent multiple timers from starting at once.
 */
function delay() {
  Drupal.settings.comment_timer.semaphore++;
}

/**
 * Reset all counters, starting the timer over at 00:00:00.
 */
function comment_timer_reset() {
  Drupal.settings.comment_timer.seconds = 0;
  comment_timer_display();
}

/**
 * Update the edit-elapsed textfield with the current elapsed time.
 */
function comment_timer_display() {
  ds = Drupal.settings.comment_timer.seconds % 60;
  dm = Math.floor(Drupal.settings.comment_timer.seconds / 60);
  dh = 0;
  if (dm > 59) {
    dh = Math.floor(dm / 60);
    dm = dm % 60;
  }
  if (dh < 10) {
    dt = '0';
  }
  else {
    dt = '';
  }
  dt += dh;
  if (dm < 10) {
    dt += ':0' + dm;
  }
  else {
    dt += ':' + dm;
  }
  if (ds < 10) {
    dt += ':0' + ds;
  }
  else {
    dt += ':' + ds;
  }
  $(document).ready(function() {
    $("#edit-comment-timer-elapsed").val(dt);
  });
}

/**
 * Count seconds.
 */
function comment_timer_timer() {
  if (!Drupal.settings.comment_timer.running) {
    return false;
  }
  patt = /^([0-9]{2}):([0-5][0-9]):([0-5][0-9])$/;
  result = patt.exec($("#edit-comment-timer-elapsed").val());
  if (result != null) {
    Drupal.settings.comment_timer.seconds = Math.round(result[1]) * 3600 + Math.round(result[2]) * 60 + Math.round(result[3]);
  }
  Drupal.settings.comment_timer.seconds += 1;
  comment_timer_display();
  window.setTimeout(function() {
    comment_timer_timer();
  }, 1000);
}

/**
 * Start the timer when the page is fully loaded.
 */
Drupal.behaviors.comment_timer = function() {
  Drupal.settings.comment_timer.seconds = Math.floor(Drupal.settings.comment_timer.seconds);
  Drupal.settings.comment_timer.running = 1;
  Drupal.settings.comment_timer.semaphore = 0;
  comment_timer_timer();
  $("#edit-comment-timer-pause").bind('click', function() {
    if (Drupal.settings.comment_timer.running) {
      Drupal.settings.comment_timer.running = 0;
      window.setTimeout(function() {
        delay();
      }, 1000);
      $(this).val(Drupal.t('Run'));
    }
    else {
      if (Drupal.settings.comment_timer.semaphore) {
        Drupal.settings.comment_timer.running = 1;
        Drupal.settings.comment_timer.semaphore = 0;
        comment_timer_timer();
      }
      $(this).val(Drupal.t('Pause'));
    }
    return false;
  });
  $("#edit-comment-timer-reset").bind('click', function() {
    Drupal.settings.comment_timer.seconds = 0;
    comment_timer_display();
    return false;
  });
}
